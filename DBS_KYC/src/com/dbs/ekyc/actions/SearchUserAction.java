/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dbs.ekyc.actions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.FaceMatch;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.S3Object;
import com.amazonaws.services.rekognition.model.SearchFacesByImageRequest;
import com.amazonaws.services.rekognition.model.SearchFacesByImageResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.dbs.ekyc.aws.DynamoAccessor;
import com.dbs.ekyc.aws.KycInfo;
import com.dbs.ekyc.forms.RegistrationForm;
import com.dbs.ekyc.forms.SearchForm;

import org.apache.struts.action.ActionForward;

/**
 *
 * @author eswar@vaannila.com
 */
public class SearchUserAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private final static String DBS_HOME = "home";
    private final static String SEARCH_SUCCESS = "searchsuccess";
    private final static String SEARCH_FAILURE = "searchfailure";
    private static String bucketName     = "dbskyc";
    
    public static final String COLLECTION_ID = "DBSKYC";
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    		
    	SearchForm searchForm = (SearchForm) form;
    	
    	
    	 FormFile formFile = searchForm.getImage();
         String path = getServlet().getServletContext().getRealPath("")+"/"+
         formFile.getFileName();
         
         
         FileOutputStream outputStream = new FileOutputStream(new File(path));
         outputStream.write(formFile.getFileData());
         outputStream.close();
         
         
         String keyName = "searchuser"+formFile.getFileName().substring(formFile.getFileName().indexOf("."));
         
         uploadImage(new File(path),keyName);
         
        Image img = getImageUtil(bucketName,keyName);
        
        Float threshold = 70F;
        int maxFaces = 2;
        
        
        AWSCredentials credentials = new AWSCredentials() {
			  public String getAWSSecretKey() { return "Ww/RaY5enzynqgLnVzFGvtfHU7TPI+Y3ZW5Ldf76"; }
			  public String getAWSAccessKeyId() { return "AKIAISKEIXP6DLDAZYBQ"; }
			};


        AmazonRekognition amazonRekognition = AmazonRekognitionClientBuilder
           .standard()
           .withRegion(Regions.US_EAST_1)
           .withCredentials(new AWSStaticCredentialsProvider(credentials))
           .build();
        
        SearchFacesByImageResult searchFacesByImageResult =
                callSearchFacesByImage(COLLECTION_ID, img, threshold, maxFaces,
                   amazonRekognition);
             KycInfo kycInfo = null;
             System.out.println("Faces matching largest face in image  " + keyName);
             List < FaceMatch > faceImageMatches = searchFacesByImageResult.getFaceMatches();
             for (FaceMatch face: faceImageMatches) {
                 String kycId = face.getFace().getFaceId();
            	 System.out.println("Search Result Face ID = "+face.getFace().getFaceId());
            	  kycInfo = DynamoAccessor.getKycInfo(kycId);
            	 
            	 if(kycInfo!=null)
            	 {
            		 System.out.println(kycInfo);
            		 break;
            	 }
                
             }
             
             request.getSession().setAttribute("userdata", kycInfo);
             if(kycInfo!=null)
             return mapping.findForward(SEARCH_SUCCESS);
             else
            	 return mapping.findForward(SEARCH_FAILURE);
             
       
    }
    
    
    
    private static Image getImageUtil(String bucket, String key) {
        return new Image()
           .withS3Object(new S3Object()
              .withBucket(bucket)
              .withName(key));
     }
    
    
    private static SearchFacesByImageResult callSearchFacesByImage(String collectionId,
    	      Image image, Float threshold, int maxFaces, AmazonRekognition amazonRekognition
    	   ) {
    	      SearchFacesByImageRequest searchFacesByImageRequest = new SearchFacesByImageRequest()
    	         .withCollectionId(collectionId)
    	         .withImage(image)
    	         .withFaceMatchThreshold(threshold)
    	         .withMaxFaces(maxFaces);
    	      return amazonRekognition.searchFacesByImage(searchFacesByImageRequest);
    	   }
    
    
    
    
    
 private void uploadImage(File file,String keyName) {
		
		@SuppressWarnings("deprecation")
		AWSCredentials credentials = new AWSCredentials() {
			  public String getAWSSecretKey() { return "Ww/RaY5enzynqgLnVzFGvtfHU7TPI+Y3ZW5Ldf76"; }
			  public String getAWSAccessKeyId() { return "AKIAISKEIXP6DLDAZYBQ"; }
			};
			
		AmazonS3 s3client = new AmazonS3Client(new AWSStaticCredentialsProvider(credentials));
        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            PutObjectResult res = s3client.putObject(new PutObjectRequest(
            		                 bucketName, keyName, file));
            System.out.println("test");

         } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }
}
