/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dbs.ekyc.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.apache.struts.action.ActionForward;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.FaceRecord;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.IndexFacesRequest;
import com.amazonaws.services.rekognition.model.IndexFacesResult;
import com.amazonaws.services.rekognition.model.ListFacesRequest;
import com.amazonaws.services.rekognition.model.ListFacesResult;
import com.amazonaws.services.rekognition.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.dbs.ekyc.aws.AwsUtils;
import com.dbs.ekyc.aws.DynamoAccessor;
import com.dbs.ekyc.aws.KycInfo;
import com.dbs.ekyc.forms.RegistrationForm;

/**
 *
 * @author eswar@vaannila.com
 */
public class RegisterUserAction extends org.apache.struts.action.Action {

	/* forward name="success" path="" */
	private final static String REG_SUCCESS = "registrationsuccess";
	private final static String DBS_FALIED = "failure";

	private static String bucketName = "dbskyc";
	private String keyName = "";
	private String keyNamewithoutextn = "";

	public static final String COLLECTION_ID = "DBSKYC";
	public static final String S3_BUCKET = "dbskyc";

	/**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RegistrationForm registrationForm = (RegistrationForm) form;
       
        
        FormFile formFile = registrationForm.getImage();
        String path = getServlet().getServletContext().getRealPath("")+"/"+
        formFile.getFileName();
        
        
        FileOutputStream outputStream = new FileOutputStream(new File(path));
        outputStream.write(formFile.getFileData());
        outputStream.close();
        
        
        keyNamewithoutextn=registrationForm.getFirstName().replace(" ", "")+"_"+registrationForm.getLastName().replace(" ", "")+"_"+System.currentTimeMillis();
        keyName = keyNamewithoutextn+formFile.getFileName().substring(formFile.getFileName().indexOf("."));
        
        uploadImage(new File(path));
        String kycId = getUserFaceId();
        
        if(!isAlreadyRegistered(kycId)) {
        
        DynamoAccessor.saveKycInfo(registrationForm.getAddress(), registrationForm.getAddresscode(), 
        		registrationForm.getCity(), registrationForm.getCountryform(), registrationForm.getDob(), 
        		registrationForm.getRelation(), registrationForm.getRelationName(), registrationForm.getFirstName(), 
        		registrationForm.getGender(), kycId, registrationForm.getLastName(), keyName, registrationForm.getStateform());
        
        }
        
        request.getSession().setAttribute("success","User details successfully submitted");
            return mapping.findForward(REG_SUCCESS);
        
            
       
    }
    
    
public  boolean isAlreadyRegistered(String kycId) {
        
        DynamoDBMapper mapper = AwsUtils.initializeDynamoDBMapper();

        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":val1", new AttributeValue().withS(kycId));

        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression()
                .withFilterExpression("KycId = :val1")
                .withExpressionAttributeValues(eav);

        List<KycInfo> scanResult = mapper.scan(KycInfo.class, scanExpression);
        
        if (scanResult.size()>0)
          return true;
        
        
        return false;
        
    }

	private void uploadImage(File file) {

		@SuppressWarnings("deprecation")
		
		AWSCredentials credentials = new AWSCredentials() {
			  public String getAWSSecretKey() { return "Ww/RaY5enzynqgLnVzFGvtfHU7TPI+Y3ZW5Ldf76"; }
			  public String getAWSAccessKeyId() { return "AKIAISKEIXP6DLDAZYBQ"; }
			}; 
			AmazonS3 s3client = new AmazonS3Client(credentials);
		try {
			System.out.println("Uploading a new object to S3 from a file\n");
			PutObjectResult res = s3client.putObject(new PutObjectRequest(bucketName, keyName, file));
			System.out.println("test");

		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which " + "means your request made it "
					+ "to Amazon S3, but was rejected with an error response" + " for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which " + "means the client encountered "
					+ "an internal error while trying to " + "communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
	}

	private static IndexFacesResult callIndexFaces(String collectionId, String externalImageId, String attributes,
			Image image, AmazonRekognition amazonRekognition) {
		IndexFacesRequest indexFacesRequest = new IndexFacesRequest().withImage(image).withCollectionId(collectionId)
				.withExternalImageId(externalImageId).withDetectionAttributes(attributes);
		return amazonRekognition.indexFaces(indexFacesRequest);

	}

	@SuppressWarnings("unused")
	private static ListFacesResult callListFaces(String collectionId, int limit, String paginationToken,
			AmazonRekognition amazonRekognition) {
		ListFacesRequest listFacesRequest = new ListFacesRequest().withCollectionId(collectionId).withMaxResults(limit)
				.withNextToken(paginationToken);
		return amazonRekognition.listFaces(listFacesRequest);
	}

	private static Image getImageUtil(String bucket, String key) {
		return new Image().withS3Object(new S3Object().withBucket(bucket).withName(key));
	}

	public String getUserFaceId() throws Exception {
		AWSCredentials credentials = new AWSCredentials() {
			  public String getAWSSecretKey() { return "Ww/RaY5enzynqgLnVzFGvtfHU7TPI+Y3ZW5Ldf76"; }
			  public String getAWSAccessKeyId() { return "AKIAISKEIXP6DLDAZYBQ"; }
			};

		ObjectMapper objectMapper = new ObjectMapper();

		AmazonRekognition amazonRekognition = AmazonRekognitionClientBuilder.standard().withRegion(Regions.US_EAST_1)
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).build();

		// 1. Index face 1
		Image image = getImageUtil(S3_BUCKET, keyName);
		String externalImageId = keyNamewithoutextn;
		IndexFacesResult indexFacesResult = callIndexFaces(COLLECTION_ID, externalImageId, "ALL", image,
				amazonRekognition);
		System.out.println(externalImageId + " added");
		List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
		for (FaceRecord faceRecord : faceRecords) {
			return faceRecord.getFace().getFaceId();
		}
		return "";
	}

}
