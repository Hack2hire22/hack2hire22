/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dbs.ekyc.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.dbs.ekyc.forms.CompanyRegistrationForm;
import com.dbs.ekyc.forms.RegistrationForm;

import org.apache.struts.action.ActionForward;

/**
 *
 * @author eswar@vaannila.com
 */
public class RegisterCompanyAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private final static String DBS_HOME = "home";
    private final static String DBS_FALIED = "failure";
    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        CompanyRegistrationForm companyRegistrationForm = (CompanyRegistrationForm) form;
        
            return mapping.findForward(DBS_HOME);
       
    }
}
