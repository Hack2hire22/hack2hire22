package com.dbs.ekyc.aws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class DynamoAccessor {

	public static KycInfo getKycInfo(String kycId) {

		DynamoDBMapper mapper = AwsUtils.initializeDynamoDBMapper();

		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS(kycId));

		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("KycId = :val1")
				.withExpressionAttributeValues(eav);

		List<KycInfo> scanResult = mapper.scan(KycInfo.class, scanExpression);
		for (KycInfo kycInfo : scanResult) {
			return kycInfo;
		}
		
		return null;

	}

	public static void saveKycInfo(String Address, String AddressCode, String City, String Country, String DOB,
			String FamilyLink, String FamilyLinkName, String FirstName, String Gender, String KycId, String LastName,
			String PhotoDownloadLink, String State) {

		KycInfo kycInfo = new KycInfo();

		kycInfo.setAddress(Address);
		kycInfo.setAddressCode(AddressCode);
		kycInfo.setCity(City);
		kycInfo.setCountry(Country);
		kycInfo.setDOB(DOB);
		kycInfo.setFamilyLink(FamilyLink);
		kycInfo.setFamilyLinkName(FamilyLinkName);
		kycInfo.setFirstName(FirstName);
		kycInfo.setGender(Gender);
		kycInfo.setKycId(KycId);
		kycInfo.setLastName(LastName);
		kycInfo.setPhotoDownloadLink(PhotoDownloadLink);
		kycInfo.setState(State);

		DynamoDBMapper mapper = AwsUtils.initializeDynamoDBMapper();
		mapper.save(kycInfo);

	}

}
