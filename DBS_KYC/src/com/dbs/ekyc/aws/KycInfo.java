package com.dbs.ekyc.aws;



import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="KycInformation")
public class KycInfo {
    

private String KycId;
@DynamoDBHashKey(attributeName="KycId")

public String getKycId() {
	return KycId;
}

public void setKycId(String kycId) {
	KycId = kycId;
}

public String getFirstName() {
	return FirstName;
}

public void setFirstName(String firstName) {
	FirstName = firstName;
}

public String getAddress() {
	return Address==null?"":Address;
}

public void setAddress(String address) {
	Address = address;
}

public String getAddressCode() {
	return AddressCode==null?"":AddressCode;
}

public void setAddressCode(String addressCode) {
	AddressCode = addressCode;
}

public String getCity() {
	return City==null?"":City;
}

public void setCity(String city) {
	City = city;
}

public String getCountry() {
	if (Country == null){
		return "";
	}
	return Country.equalsIgnoreCase("-1")?"":Country;
}

public void setCountry(String country) {
	Country = country;
}

public String getDOB() {
	return DOB==null?"":DOB;
}

public void setDOB(String dOB) {
	DOB = dOB;
}

public String getFamilyLink() {
	return FamilyLink==null?"":FamilyLink;
}

public void setFamilyLink(String familyLink) {
	FamilyLink = familyLink;
}

public String getFamilyLinkName() {
	return FamilyLinkName==null?"":FamilyLinkName;
}

public void setFamilyLinkName(String familyLinkName) {
	FamilyLinkName = familyLinkName;
}

public String getGender() {
	return Gender;
}

public void setGender(String gender) {
	Gender = gender;
}

public String getLastName() {
	return LastName;
}

public void setLastName(String lastName) {
	LastName = lastName;
}

public String getPhotoDownloadLink() {
	return PhotoDownloadLink;
}

public void setPhotoDownloadLink(String photoDownloadLink) {
	PhotoDownloadLink = photoDownloadLink;
}

public String getState() {
	return State==null?"":State;
}

public void setState(String state) {
	State = state;
}

@DynamoDBAttribute(attributeName="FirstName")
private String FirstName;

@DynamoDBAttribute(attributeName="Address")
private String Address;

@DynamoDBAttribute(attributeName="AddressCode")
private String AddressCode;

@DynamoDBAttribute(attributeName="City")
private String City;

@DynamoDBAttribute(attributeName="Country")
private String Country;

@DynamoDBAttribute(attributeName="DOB")
private String DOB;

@DynamoDBAttribute(attributeName="FamilyLink")
private String FamilyLink;

@DynamoDBAttribute(attributeName="FamilyLinkName")
private String FamilyLinkName;

@DynamoDBAttribute(attributeName="Gender")
private String Gender;

@DynamoDBAttribute(attributeName="LastName")
private String LastName;

@DynamoDBAttribute(attributeName="PhotoDownloadLink")
private String PhotoDownloadLink;

@DynamoDBAttribute(attributeName="State")
private String State;
@Override
public String toString() {
	return "KycInfo [KycId=" + KycId + ", FirstName=" + FirstName + ", Address=" + Address + ", AddressCode="
			+ AddressCode + ", City=" + City + ", Country=" + Country + ", DOB=" + DOB + ", FamilyLink=" + FamilyLink
			+ ", FamilyLinkName=" + FamilyLinkName + ", Gender=" + Gender + ", LastName=" + LastName
			+ ", PhotoDownloadLink=" + PhotoDownloadLink + ", State=" + State + "]";
}





}