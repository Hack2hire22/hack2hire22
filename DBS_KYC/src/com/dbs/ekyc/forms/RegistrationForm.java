/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dbs.ekyc.forms;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author eswar@vaannila.com
 */
public class RegistrationForm extends org.apache.struts.action.ActionForm {
    
    private String firstName;

    private String lastName;
    
    private String gender;
    
    private String countryform;
    
    private String stateform;
    
    private String city;
    
    private String address;
    
    private String addresscode;
    
    private String dob;
    
    private String relation;
    
    private String relationName;
    

	private FormFile image;
    
    

    
    /**
     *
     */
    public RegistrationForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
    
        return errors;
    }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountryform() {
		return countryform;
	}

	public void setCountryform(String countryform) {
		this.countryform = countryform;
	}

	public String getStateform() {
		return stateform;
	}

	public void setStateform(String stateform) {
		this.stateform = stateform;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddresscode() {
		return addresscode;
	}

	public void setAddresscode(String addresscode) {
		this.addresscode = addresscode;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}

	public FormFile getImage() {
		return image;
	}

	public void setImage(FormFile image) {
		this.image = image;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	@Override
	public String toString() {
		return "RegistrationForm [firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender
				+ ", country=" + countryform + ", state=" + stateform + ", city=" + city + ", address=" + address
				+ ", addresscode=" + addresscode + ", dob=" + dob + ", relation=" + relation + ", relationName="
				+ relationName + ", image=" + image + "]";
	}

	
	
	
   
    
    
    
    
}
