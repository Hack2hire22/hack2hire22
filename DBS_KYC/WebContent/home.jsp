<%-- 
    Document   : success
    Created on : Dec 15, 2008, 4:08:53 AM
    Author     : eswar@vaannila.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


 <html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DBS-KYC</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="DBS Hack2Hire" />
	<meta name="keywords" content="dbs hack events" />
	<meta name="author" content="theja" />



<link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,900' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
	
	
	<link rel="stylesheet" href="css/animate.css">
	
	<link rel="stylesheet" href="css/icomoon.css">
	
	<link rel="stylesheet" href="css/bootstrap.css">
	                  
	<link rel="stylesheet" href="css/superfish.css">
	
	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">


	
	<script src="js/modernizr-2.6.2.min.js"></script>
	
	<style>

</style>

	</head>
	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div id="fh5co-header">
			<header id="fh5co-header-section">
				<div class="container">
					<div class="nav-header">
						<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a> 
						<h1 id="fh5co-logo"><a href="home.jsp"> <img style="width:40%"src="dbslogo.png"  alt="" /></a></h1>

						<nav id="fh5co-menu-wrap" role="navigation">
							<ul class="sf-menu" id="fh5co-primary-menu">
								<li>
									<a class="active" href="home.jsp">Home</a>
								</li>
							
							     <li><a href="registeruser.jsp">Register KYC</a></li>	
								<li><a href="searchuser.jsp">Search kYC</a></li>
								
								<li><a href="team.jsp">Team</a></li>
								
								
							</ul>
						</nav>
					</div>
				</div>
			</header>		
		</div>
		
		<aside id="fh5co-hero" class="js-fullheight">
			<div class="flexslider js-fullheight">
				<ul class="slides">
			   	<li style="background-image: url(images/slide_1.jpg);">
			   		<div class="overlay-gradient"></div>
			   		<div class="container">
			   			<div class="col-md-5 col-sm-6 col-xs-12 js-fullheight slider-text">
			   				<div class="slider-text-inner">
			   					<div class="desc">
			   						<h2>Hack2Hire Event 2017 ,Hyderabad </h2>
			   						 <br>
									 
								 
			   					</div>
			   				</div>
			   			</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/slide_2.jpg);">
			   		<div class="overlay-gradient"></div>
			   		<div class="container">
			   			<div class="col-md-5 col-sm-6 col-xs-12 js-fullheight slider-text">
			   				<div class="slider-text-inner">
			   					<div class="desc">
			   						<h2>Hack2Hire Event 2017 ,Hyderabad </h2>
			   						<br><br>
			   					</div>
			   				</div>
			   			</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/slide_3.jpg);">
			   		<div class="overlay-gradient"></div>
			   		<div class="container">
			   			<div class="col-md-5 col-sm-6 col-xs-12 js-fullheight slider-text">
			   				<div class="slider-text-inner">
			   					<div class="desc">
			   						<h2>Hack2Hire Event 2017 ,Hyderabad </h2>
			   						<br><br>
			   					</div>
			   				</div>
			   			</div>
			   		</div>
			   	</li>
							  
					 
			  	</ul>
		  	</div>
		</aside>
		
		
		<footer>
			<div id="footer">
				<div class="container">
					<div class="row">
					<h1 class="text-center">About DBS - KYC</h1> 
						
						<div >	
						<font color="black"	>
						KYC means “Know Your Customer”. It is a process by which banks or any other entity obtain information about the 
identity and address of the customers. This process helps to ensure that services are not misused. The 
KYC procedure is to be completed by the banks while opening accounts and is also used by other services to identify a customer. 
A periodical update of the customers’ KYC details is required to ensure that information is valid.

Refugees Contribution in Economy:

It is a general perception that Refugees are transitory, however according to recent surveys conducted by CNN 
and The Huffington Post, that the average length of stay in a refugee camp is 17 years. That makes the Refugees
financial inclusion very critical factor in a country's economic growth.
<br>
<b><u>Our Approach:</u></b>
<br>
We have build an Refugee KYC registration application that can be integrated with any government entity and help refugees in getting financial assistance from the government.
Our application serves in two ways,<br>1. Web application and <br>2. iOS application <br>and thus will reach to large audience.
<br>
<b><u>Workflow for Onboarding:</u></b>
<br>
Refugees will get onboard to DBS-KYC through our application by providing basic informations and their image. The data will be stored in DynamoDB and the image is uploaded in S3 Bucket.
A face ID will be generated through Amazon Rekognition Service and this face ID will be served as primary key for that record.
<br>
<b><u>Workflow for Identification:</u></b>
<br>
Refugee's image will be captured and corresponding face ID will be generated. This face ID is then matched against our database and the information of that Refugee will be fetched.
This information is then displayed in our portal.
  

</font>
</div>








  </div>

		</div>
		</div>
		</footer>

<div class="row copy-right  down">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="https://www.linkedin.com/in/saithejagonavaram" target="_blank"><i class="icon-linkedin2"></i></a>
								<a href="https://www.facebook.com/saiteja.gonavaram" target="_blank"><i class="icon-facebook2"></i></a>
								
							</p>
							<p>&copy; SaiTheja Gonavaram.  All Rights Reserved. </p></div>
					</div>

  </div>

	

<script>
			 
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );
		</script>
	<script src="js/jquery.min.js"></script>
	
	<script src="js/jquery.easing.1.3.js"></script>
	
	<script src="js/bootstrap.min.js"></script>
	
	<script src="js/jquery.waypoints.min.js"></script>
	
	<script src="js/hoverIntent.js"></script>
	<script src="js/superfish.js"></script>
	
	<script src="js/jquery.flexslider-min.js"></script>

	
	<script src="js/main.js"></script>
  </head>
	</body>
</html>

