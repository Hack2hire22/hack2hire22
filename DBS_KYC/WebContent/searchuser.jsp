<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DBS-KYC</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="DBS Hack2Hire" />
	<meta name="keywords" content="dbs hack events" />
	<meta name="author" content="theja" />



<link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,900' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
	
	
	<link rel="stylesheet" href="css/animate.css">
	
	<link rel="stylesheet" href="css/icomoon.css">
	
	<link rel="stylesheet" href="css/bootstrap.css">
	                  
	<link rel="stylesheet" href="css/superfish.css">
	
	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">


	
	<script src="js/modernizr-2.6.2.min.js"></script>
	
	<style>
	.div1 {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
	</style>
	
<script>
  
  
  
  
  function validate()
  {
	  var image = document.forms[0].image.value;
	  var ext = image.substring(image.indexOf(".")+1);
	  
	  
	 
	  if(ext!="jpg" && ext!="png" && ext!="JPG" && ext!="PNG")
	  {
	  alert("Please upload image only");
      return false;
	  }
  
	  
	  
	  
	  return true;
  }


</script>
	</head>
	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div id="fh5co-header">
			<header id="fh5co-header-section">
				<div class="container">
					<div class="nav-header">
						<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a> 
						<h1 id="fh5co-logo"><a href="index.html"> <img style="width:70%"src="dbslogo.png"  alt="" /></a></h1>

						<nav id="fh5co-menu-wrap" role="navigation">
							<ul class="sf-menu" id="fh5co-primary-menu">
								<li>
									<a class="active" href="index.html">Home</a>
								</li>
							
							     <li><a href="registeruser.jsp">Register KYC</a></li>	
								<li><a href="searchuser.jsp">Search kYC</a></li>
								
								<li><a href="team.jsp">Team</a></li>
								
							</ul>
						</nav>
					</div>
				</div>
			</header>		
		</div>
		
      		<footer>
			
              <div id="footer">
				<div class="container">
					<div class="row">
					


						<div class="col-md-6 abts">
						<br><h1 class="text-center">Upload Image to get Details</h1> 
						

                        <div class="div1">
                        <html:form action="/SearchUser" enctype="multipart/form-data" method="post">

                         <label for="image"> Upload Image*</label> <br><html:file  name="SearchForm" property="image" /><br>
         
                      <html:submit value="Submit" onclick="javascript : return validate();"/>

                        <div>

                         



                        </div>



                        </html:form>

					    </div>

                        </div>
                  </div>

</div>
</div>



</footer>

<section>
<div class="row copy-right  down">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="https://www.linkedin.com/in/saithejagonavaram" target="_blank"><i class="icon-linkedin2"></i></a>
								<a href="https://www.facebook.com/saiteja.gonavaram" target="_blank"><i class="icon-facebook2"></i></a>
								
							</p>
							<p>&copy; SaiTheja Gonavaram.  All Rights Reserved. </p></div>
					</div>

  </div>
  </section>

</div>
</div>
<script language="javascript">
	populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
	populateCountries("country2");
	populateCountries("country2");
</script>


</head>
</body>
</html>


