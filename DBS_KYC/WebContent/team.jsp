
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DBS-KYC</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="DBS_KYC" />
	<meta name="keywords" content="DBS_KYC"/>
	<meta name="author" content="saithejagonavaram" />
<link rel="stylesheet" href="style.css" />
<link rel="stylesheet" href="style1.css" />


<link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,900' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
	<style>
	BODY{ background-color: #FA7E0A;}
	 .spon h1 {
  font-family: "Avant Garde", Avantgarde, "Century Gothic", CenturyGothic, "AppleGothic", sans-serif;
  font-size: 92px;
  padding: 0px 	0px;
  text-align: center;
  text-transform: uppercase;
  text-rendering: optimizeLegibility;
}
h1.elegantshadow {
  color: #131313;
  background-color: #e7e5e4;
  letter-spacing: .15em;
  text-shadow: 1px -1px 0 #767676, -1px 2px 1px #737272, -2px 4px 1px #767474, -3px 6px 1px #787777, -4px 8px 1px #7b7a7a, -5px 10px 1px #7f7d7d, -6px 12px 1px #828181, -7px 14px 1px #868585, -8px 16px 1px #8b8a89, -9px 18px 1px #8f8e8d, -10px 20px 1px #949392, -11px 22px 1px #999897, -12px 24px 1px #9e9c9c, -13px 26px 1px #a3a1a1, -14px 28px 1px #a8a6a6, -15px 30px 1px #adabab, -16px 32px 1px #b2b1b0, -17px 34px 1px #b7b6b5, -18px 36px 1px #bcbbba, -19px 38px 1px #c1bfbf, -20px 40px 1px #c6c4c4, -21px 42px 1px #cbc9c8, -22px 44px 1px #cfcdcd, -23px 46px 1px #d4d2d1, -24px 48px 1px #d8d6d5, -25px 50px 1px #dbdad9, -26px 52px 1px #dfdddc, -27px 54px 1px #e2e0df, -28px 56px 1px #e4e3e2;
}
.wrks{
 
	background-image: url('images/pattern.png');
	background-repeat: repeat;
	background-color: rgba(44,62,80 , 0.1 );
	background-attachment:fixed;	}
.txtf{
text-align:center;}

.txtf{
 font: normal 18px "Bree Serif",serif;
word-spacing:5px;

 color:black;}
	</style>


	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="css/superfish.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<style type="text/css">
.txtf{padding-top:240px;}
</style>
<body>

<div id="fh5co-wrapper">
		<div id="fh5co-page">
<div id="fh5co-header">
			<header id="fh5co-header-section">
				<div class="container">
					<div class="nav-header">
						<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a> 
						<h1 id="fh5co-logo"><a href="home.jsp"> <img style="width:40%"src="dbslogo.png"  alt="" /></a></h1>

						<nav id="fh5co-menu-wrap" role="navigation">
							<ul class="sf-menu" id="fh5co-primary-menu">
								<li>
									<a class="active" href="home.jsp">Home</a>
								</li>
							
							     <li><a href="registeruser.jsp">Register KYC</a></li>	
								<li><a href="searchuser.jsp">Search kYC</a></li>
								
								<li><a href="team.jsp">Team</a></li>
								
							</ul>
						</nav>
					</div>
				</div>
			</header>
		</div>
		
		
    <div class="wrks">
	<div class="container txtf">
	<h1>UI Developer</h1>
	<div class="row"><div class="col-md-4"></div> <div class="col-md-4"><img src="images/teja.jpg"  height="200" width="250" alt="" /></div> <div class="col-md-4"></div></div>
		<a >Venkata Sai Theja Gonavaram</a>
		
        	<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="https://www.linkedin.com/in/saithejagonavaram" target="_blank"><i class="icon-linkedin2"></i></a>
								<a href="https://www.facebook.com/saiteja.gonavaram" target="_blank"><i class="icon-facebook2"></i></a>
								
							</p>
							<hr>
		<h1> AWS Developer</h1>
	<div class="row"><div class="col-md-4"></div> <div class="col-md-4"><img src="images/akash.png" height="200" width="200" alt="" /></div> <div class="col-md-4"></div></div>
		<a >Akash Srivastava</a>
		
        	<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="http://linkedin.com/in/akash1684" target="_blank"><i class="icon-linkedin2"></i></a>
								
								
							</p>
							<hr>

		

		<h1>Facial Recognition And IOS Developer</h1>
	<div class="row"><div class="col-md-4"></div> <div class="col-md-4"><img src="images/abhigyan.jpg" height="250" width="250" alt="" /></div> <div class="col-md-4"></div></div>
		<a > Abhigyan Singh</a>
		
        	<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="http://linkedin.com/in/abhigyan2311" target="_blank"><i class="icon-linkedin2"></i></a>
								<a href="https://www.facebook.com/abhigyan2311" target="_blank"><i class="icon-facebook2"></i></a>
								
							</p>
							<hr>


		<h1>J2EE Developer</h1>
	<div class="row"><div class="col-md-4"></div> <div class="col-md-4"><img src="images/surya.jpg" height="250" width="250" alt="" /></div> <div class="col-md-4"></div></div>
		<a >Surya Chandra Sekhar</a>
		
       
							<hr>



		<h1>J2EE Developer</h1>
	<div class="row"><div class="col-md-4"></div> <div class="col-md-4"><img src="images/ykumara.jpg" height="250" width="250" alt="" /></div> <div class="col-md-4"></div></div>
		<a >Kumara Swamy yadha</a>
		
        	
							<hr>






	
	
	
	</div>	
	</div>
	</div>
	</div>
		<footer>
			
				
						
					
						
						

			<div class="row copy-right  down">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="https://www.linkedin.com/in/saithejagonavaram" target="_blank"><i class="icon-linkedin2"></i></a>
								<a href="https://www.facebook.com/saiteja.gonavaram" target="_blank"><i class="icon-facebook2"></i></a>
								
							</p>
							<p>&copy; SaiTheja Gonavaram.  All Rights Reserved. </p></div>
					</div>

  </div>				
				</div>
			</div>
		</footer>
	
</body>
</html>