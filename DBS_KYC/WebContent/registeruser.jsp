<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DBS-KYC</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="DBS Hack2Hire" />
	<meta name="keywords" content="dbs hack events" />
	<meta name="author" content="theja" />



<link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,900' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
	
	
	<link rel="stylesheet" href="css/animate.css">
	
	<link rel="stylesheet" href="css/icomoon.css">
	
	<link rel="stylesheet" href="css/bootstrap.css">
	                  
	<link rel="stylesheet" href="css/superfish.css">
	
	<link rel="stylesheet" href="css/flexslider.css">

	<link rel="stylesheet" href="css/style.css">


	
	<script src="js/modernizr-2.6.2.min.js"></script>
	
	<style>
input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}

.div1 {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}



</style>
	
<script>
 
  
  function validate()
  {
	  var image = document.forms[0].image.value;
	  var ext = image.substring(image.indexOf(".")+1);
	  
	  
	  if(document.forms[0].firstName.value==null || document.forms[0].firstName.value=="")
	  {
	  alert("Please enter first name");
      return false;
	  }
	  
	  if(document.forms[0].lastName.value==null || document.forms[0].lastName.value=="")
	  {
	  alert("Please enter last name");
      return false;
	  }
	  
	  if(document.forms[0].gender.value==null || document.forms[0].gender.value=="")
	  {
	  alert("Please enter gender");
      return false;
	  }
	  
	  
	  if(document.forms[0].gender.value!="M" && document.forms[0].gender.value!="F")
	  {
	  alert("Please enter M or F for gender");
      return false;
	  }
	  
	  
	  if(ext!="jpg" && ext!="png" && ext!="JPG" && ext!="PNG")
	  {
	  alert("Please upload image only");
      return false;
	  }
  
	  
	  
	  document.forms[0].dob.value = document.getElementById("bday").value;
	  document.forms[0].countryform.value = document.getElementById("country").value;
	  document.forms[0].stateform.value = document.getElementById("state").value;
	  return true;
  }

</script>
	</head>
	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div id="fh5co-header">
			<header id="fh5co-header-section">
				<div class="container">
					<div class="nav-header">
						<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a> 
						<h1 id="fh5co-logo"><a href="home.jsp"> <img style="width:40%"src="dbslogo.png"  alt="" /></a></h1>

						<nav id="fh5co-menu-wrap" role="navigation">
							<ul class="sf-menu" id="fh5co-primary-menu">
								<li>
									<a class="active" href="home.jsp">Home</a>
								</li>
							
							     <li><a href="registeruser.jsp">Register KYC</a></li>	
								<li><a href="searchuser.jsp">Search kYC</a></li>
								
								<li><a href="team.jsp">Team</a></li>
								
								
							</ul>
						</nav>
					</div>
				</div>
			</header>		
		</div>
		
      		<footer>
			
              <div id="footer">
				<div class="container">
					<div class="row">
					


						<div class="col-md-6 abts">
						<br><br><h1 class="text-center">Please Fill In The Details</h1> 
						

                        <div class="div1">
                        <html:form action="/Register" enctype="multipart/form-data" method="post">

                          
              <br> <label >First Name*</label><br> <html:text  name="RegistrationForm" property="firstName" /> <br>
                        <label >Last Name*</label>
                        <br><html:text  name="RegistrationForm" property="lastName" /> <br>
                         <label for="gender">Gender*</label>
      <br> <html:select  name="RegistrationForm" property="gender" >
               <option value="M">M</option>
                         <option value="F">F</option>
           </html:select><br>
                           <label for="dob">Date of Birth</label>
              <br><input type="date" id="bday" name="bday"><br>
                          <label for="birthplace">Birth Place:</label>
                      <br>
                                           
                         <script type= "text/javascript" src = "js/countries.js"></script>
	                      <label for="country"> Select-Country-(with-states) </label> <br>
	                      <select id="country" name ="country"></select> </br></br>
	                    <label for="state"> Select State </label>  <select name ="state" id ="state"></select>  </br> </br> <hr/>
            	        <label for="city">City</label><br> <html:text  name="RegistrationForm" property="city" /><br>
 
                       <br>
         <label for="lastknownaddress">Last Known Address</label>
       <br>
      <html:text  name="RegistrationForm" property="address" /><br>
                   
                         <br>
      <label for="family connection"> Family Connection </label> 
        <br>
                         <html:select name="RegistrationForm" property="relation">
                         <option value="father">father</option>
                         <option value="mother">mother</option>
 						 <option value="spouse">spouse</option>
						  
						</html:select><br>
                        <label for="fmilyconn"> Relation Name</label> <br>

                        <html:text  name="RegistrationForm" property="relationName" /><br>
                        <label for="image"> Upload Image*</label> <br><html:file  name="RegistrationForm" property="image" /><br>
                        <html:submit value="Submit" onclick="javascript : return validate();"/>
                        
                        <html:hidden  name="RegistrationForm" property="dob" />
                        <html:hidden  name="RegistrationForm" property="countryform" />
                        <html:hidden  name="RegistrationForm" property="stateform" />
                        
                        </html:form>

					    </div>

                        </div>
                  </div>

</div>
</div>



</footer>

<section>
<div class="row copy-right  down">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p class="fh5co-social-icons">
							<br>
								Follow me on <a href="https://www.linkedin.com/in/saithejagonavaram" target="_blank"><i class="icon-linkedin2"></i></a>
								<a href="https://www.facebook.com/saiteja.gonavaram" target="_blank"><i class="icon-facebook2"></i></a>
								
							</p>
							<p>&copy; SaiTheja Gonavaram.  All Rights Reserved. </p></div>
					</div>

  </div>
  </section>

</div>
</div>
<script language="javascript">
	populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
	populateCountries("country2");
	populateCountries("country2");
</script>


</head>
</body>
</html>


